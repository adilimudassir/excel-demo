@extends('layouts.app')
@section('content')
<div class="jumbotron">
    <form action="{{ route('import') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlFile1">Example file input</label>
            <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
        </div>
        <button type="submit">Submit</button>
    </form>
</div>
@endsection
