<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class FileImport implements ToCollection
{
    /**
    * @param Collection $row
    */
    public function collection(Collection $rows)
    {
        //this will loop through the document rows

        foreach ($rows as $rowKey => $row) {
            foreach($row as $columnKey => $column) {
                for ($i=0; $i < count($row); $i++) {
                    $row[$columnKey] = 'Mudassir';
                }
            }
        }
        return $rows;
    }
}
