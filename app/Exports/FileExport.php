<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class FileExport implements FromCollection
{
    protected $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection->count() == 1 ? $collection->first() : $collection;
    }

    public function collection()
    {
        $this->modifyCollection();
        return new Collection($this->collection);
    }

    public function modifyCollection()
    {
        $collectionToArray = $this->collection->toArray();
        foreach ($collectionToArray as $rowKey => $row) {
            //if the file has a header, skip the header and don't modify it
            if ($rowKey !== 0) {
                foreach($row as $columnKey => $column) {
                    for ($i=0; $i < count($row); $i++) {
                        $row[$columnKey] = 'Mudassir';
                    }
                }
            }
            $collectionToArray[$rowKey] = $row;
        }

        $this->collection = new Collection($collectionToArray);
    }
}
