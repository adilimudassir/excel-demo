<?php

namespace App\Http\Controllers;

use App\Exports\FileExport;
use App\Imports\FileImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function import(Request $request)
    {
        return $this->export(Excel::toCollection(new FileImport, $request->file));
    }

    public function export($collection)
    {

        return Excel::download(new FileExport($collection), 'file.xlsx');
    }
}
